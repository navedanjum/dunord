Sub testing()
Dim pattern As String
pattern = "Total"
RowCount = ActiveSheet.UsedRange.Rows.Count
Dim i As Integer

For i = 1 To RowCount
    Dim j As Integer
    For j = 1 To 1
        If Cells(i, j) = pattern Then
           Cells(i, j).EntireRow.Delete
        End If
    Next j
Next i
End Sub

Sub Test(mypattern)
Dim cell As Range

For Each cell In Selection
    If InStr(1, cell, mypattern, vbTextCompare) > 0 Then
        cell.EntireRow.Delete
    End If
Next
End Sub