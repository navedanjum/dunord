
```{r echo=FALSE}
options(repos="https://cloud.r-project.org/")
library(arules)
library(dplyr)
library(data.table)

```


1.) Determining whether the correlation among total sales and number of attendees during weekdays is positive or negative for spring 2015-16 

```{r}
#2015/16 Spring slaes
spring_1516Sales <- read.csv("2015-16Spring.csv",header=TRUE, sep=",")

#day wise slaes
mondaySales <- filter(spring_1516Sales, DAY == "Monday")
monsales <- sum (mondaySales$QUANTITY)
monsales <- round (monsales)
monsales <- as.integer(monsales)

tuesdaySales <- filter(spring_1516Sales, DAY == "Tuesday")
tuesales <- sum (tuesdaySales$QUANTITY)
tuesales <- round (tuesales)
tuesales <- as.integer(tuesales)

wednesdaySales <- filter(spring_1516Sales, DAY == "Wednesday")
wedsales <- sum (wednesdaySales$QUANTITY)
wedsales <- round (wedsales)
wedsales <- as.integer(wedsales)

thursdaySales <- filter(spring_1516Sales, DAY == "Thursday")
thursales <- sum (thursdaySales$QUANTITY)
thursales <- round (thursales)
thursales  <- as.integer(thursales )

fridaySales <- filter(spring_1516Sales, DAY == "Friday")
frisales <- sum (fridaySales$QUANTITY)
frisales <- round (frisales)
frisales  <- as.integer(frisales )

weekdaySales <- c(monsales,tuesales,wedsales,thursales,frisales)
weekdaySales
#Halls Occupation
hallOccu <- read.csv("lecture_occupancy_new.csv",header=TRUE, sep=",")

#2015/16 Spring hall occupation
allOccu1516 <- filter (hallOccu, SEMESTER == "15_16_K")

#some halls are booked several times in the same time slot
mondayOccu <- filter(allOccu1516, PAEV_ENG == "Monday")
mondayHallOccu <- distinct(mondayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
monOccu <- sum (mondayHallOccu$MAHUTAVUS)
monOccu  <- as.integer(monOccu)

tuesdayOccu <- filter(allOccu1516, PAEV_ENG == "Tuesday")
tuesdayHallOccu <- distinct(tuesdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
tueOccu <- sum (tuesdayHallOccu$MAHUTAVUS)
tueOccu  <- as.integer(tueOccu)

wednesdayOccu <- filter(allOccu1516, PAEV_ENG == "Wednesday")
wednesdayHallOccu <- distinct(wednesdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
wedOccu <- sum (wednesdayHallOccu$MAHUTAVUS)
wedOccu  <- as.integer(wedOccu)

thursdayOccu <- filter(allOccu1516, PAEV_ENG == "Thursday")
thursdayHallOccu <- distinct(thursdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
thurOccu <- sum (thursdayHallOccu$MAHUTAVUS)
thurOccu  <- as.integer(thurOccu)

fridayOccu <- filter(allOccu1516, PAEV_ENG == "Friday")
fridayHallOccu <- distinct(fridayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
friOccu <- sum (fridayHallOccu$MAHUTAVUS)
friOccu  <- as.integer(friOccu)

WeekdayOccu <- c((monOccu*0.7),(tueOccu*0.7),(wedOccu*0.7),(thurOccu*0.7),(friOccu*0.7))
#WeekdayOccu <- c((monOccu),(tueOccu),(wedOccu),(thurOccu),(friOccu))
WeekdayOccu

# Plot the bar chart.
plot(weekdaySales,type = "o",col = "red", xlab = "Day", ylab = "Total",main = "Density Co-orelation" )+lines(WeekdayOccu, type = "o", col = "blue")


```

2.) Determining whether the correlation among total sales and number of attendees during weekdays is positive or negative for Autumn 2016-17 

```{r}
#2016/17 Autumn slaes
autumn_1617Sales <- read.csv("2016-17Autumn.csv",header=TRUE, sep=",")

#day wise slaes
mondaySales <- filter(autumn_1617Sales, DAY == "Monday")
monsales <- sum (mondaySales$QUANTITY)

tuesdaySales <- filter(autumn_1617Sales, DAY == "Tuesday")
tuesales <- sum (tuesdaySales$QUANTITY)

wednesdaySales <- filter(autumn_1617Sales, DAY == "Wednesday")
wedsales <- sum (wednesdaySales$QUANTITY)
wedsales

thursdaySales <- filter(autumn_1617Sales, DAY == "Thursday")
thursales <- sum (thursdaySales$QUANTITY)

fridaySales <- filter(autumn_1617Sales, DAY == "Friday")
frisales <- sum (fridaySales$QUANTITY)

weekdaySales <- c(monsales,tuesales,wedsales,thursales,frisales)
weekdaySales
#Halls Occupation
hallOccu <- read.csv("lecture_occupancy_new.csv",header=TRUE, sep=",")

#2015/16 Spring hall occupation
allOccu1617 <- filter (hallOccu, SEMESTER == "16_17_S")

#some halls are booked several times in the same time slot
mondayOccu <- filter(allOccu1617, PAEV_ENG == "Monday")
mondayHallOccu <- distinct(mondayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
monOccu <- sum (mondayHallOccu$MAHUTAVUS)

tuesdayOccu <- filter(allOccu1617, PAEV_ENG == "Tuesday")
tuesdayHallOccu <- distinct(tuesdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
tueOccu <- sum (tuesdayHallOccu$MAHUTAVUS)

wednesdayOccu <- filter(allOccu1617, PAEV_ENG == "Wednesday")
wednesdayHallOccu <- distinct(wednesdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
wedOccu <- sum (wednesdayHallOccu$MAHUTAVUS)

thursdayOccu <- filter(allOccu1617, PAEV_ENG == "Thursday")
thursdayHallOccu <- distinct(thursdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
thurOccu <- sum (thursdayHallOccu$MAHUTAVUS)

fridayOccu <- filter(allOccu1617, PAEV_ENG == "Friday")
fridayHallOccu <- distinct(fridayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
friOccu <- sum (fridayHallOccu$MAHUTAVUS)

WeekdayOccu <- c((monOccu*0.4),(tueOccu*0.4),(wedOccu*0.4),(thurOccu*0.4),(friOccu*0.4))
WeekdayOccu
# Plot the bar chart.
plot(weekdaySales,type = "o",col = "red", xlab = "Day", ylab = "Total",main = "Density Co-orelation" )+lines(WeekdayOccu, type = "o", col = "blue")

```


3.) Determining whether the correlation among total sales and number of attendees during weekends is positive or negative for autumn 2016-17 

```{r}
autumn_1617Sales <- read.csv("2016-17Autumn.csv",header=TRUE, sep=",")

saturdaySales <- filter(autumn_1617Sales, DAY == "Saturday")
satsales <- sum (saturdaySales$QUANTITY)
satsales

sundaydaySales <- filter(autumn_1617Sales, DAY == "Sunday")
sunsales <- sum (sundaydaySales$QUANTITY)
sunsales

weekendSales <- c(satsales*0.7,sunsales*0.7)
print (weekendSales)

#Halls Occupation
hallOccu <- read.csv("lecture_occupancy_new.csv",header=TRUE, sep=",")

#2015/16 Spring hall occupation
allOccu1516 <- filter (hallOccu, SEMESTER == "16_17_S")

saturdayOccu <- filter(allOccu1516, PAEV_ENG == "Saturday")
saturdayHallOccu <- distinct(saturdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
satOccu <- sum (saturdayHallOccu$MAHUTAVUS)


sundayOccu <- filter(allOccu1516, PAEV_ENG == "Sunday")
sundayHallOccu <- distinct(sundayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
sunOccu <- sum (sundayHallOccu$MAHUTAVUS)

weekendOccu <- c(satOccu*1.2,sunOccu*1.2)

counts <- table(weekendOccu, weekendSales)
barplot(counts, main="Density Co-orelation",xlab="Week Ends", col=c("darkblue","red"), beside=TRUE)

#plot(weekendSales,type = "o",col = "red", xlab = "Day", ylab = "Total",main = "Density Co-orelation" )+lines(weekendOccu, type = "o", col = "blue")


```

4.) Determining whether the correlation among total sales and number of attendees during weekdays is positive or negative for Spring 2016-17 

```{r}
#2016/17 Autumn slaes

Spring_1617Sales <- read.csv("2016-17Spring.csv",header=TRUE, sep=",")

#day wise slaes
mondaySales <- filter(Spring_1617Sales, DAY == "Monday")
monsales <- sum (mondaySales$QUANTITY)

tuesdaySales <- filter(Spring_1617Sales, DAY == "Tuesday")
tuesales <- sum (tuesdaySales$QUANTITY)

wednesdaySales <- filter(Spring_1617Sales, DAY == "Wednesday")
wedsales <- sum (wednesdaySales$QUANTITY)

thursdaySales <- filter(Spring_1617Sales, DAY == "Thursday")
thursales <- sum (thursdaySales$QUANTITY)

fridaySales <- filter(Spring_1617Sales, DAY == "Friday")
frisales <- sum (fridaySales$QUANTITY)

weekdaySales <- c(monsales,tuesales,wedsales,thursales,frisales)
weekdaySales
#Halls Occupation
hallOccu <- read.csv("lecture_occupancy_new.csv",header=TRUE, sep=",")

#2015/16 Spring hall occupation
allOccu1617 <- filter (hallOccu, SEMESTER == "16_17_K")

#some halls are booked several times in the same time slot
mondayOccu <- filter(allOccu1617, PAEV_ENG == "Monday")
mondayHallOccu <- distinct(mondayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
monOccu <- sum (mondayHallOccu$MAHUTAVUS)

tuesdayOccu <- filter(allOccu1617, PAEV_ENG == "Tuesday")
tuesdayHallOccu <- distinct(tuesdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
tueOccu <- sum (tuesdayHallOccu$MAHUTAVUS)

wednesdayOccu <- filter(allOccu1617, PAEV_ENG == "Wednesday")
wednesdayHallOccu <- distinct(wednesdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
wedOccu <- sum (wednesdayHallOccu$MAHUTAVUS)

thursdayOccu <- filter(allOccu1617, PAEV_ENG == "Thursday")
thursdayHallOccu <- distinct(thursdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
thurOccu <- sum (thursdayHallOccu$MAHUTAVUS)

fridayOccu <- filter(allOccu1617, PAEV_ENG == "Friday")
fridayHallOccu <- distinct(fridayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
friOccu <- sum (fridayHallOccu$MAHUTAVUS)

#WeekdayOccu <- c((monOccu),(tueOccu),(wedOccu),(thurOccu),(friOccu))
#WeekdayOccu

WeekdayOccu <- c((monOccu*0.7),(tueOccu*0.7),(wedOccu*0.7),(thurOccu*0.7),(friOccu*0.7))
#WeekdayOccu
# Plot the bar chart.
plot(weekdaySales,type = "o",col = "red", xlab = "Day", ylab = "Total",main = "Density Co-orelation" )+lines(WeekdayOccu, type = "o", col = "blue")

```


3.) Determining whether the correlation among total sales and number of attendees during weekends is positive or negative for autumn 2016-17 

```{r}
Spring_1617Sales <- read.csv("2016-17Spring.csv",header=TRUE, sep=",")

saturdaySales <- filter(Spring_1617Sales, DAY == "Saturday")
satsales <- sum (saturdaySales$QUANTITY)
satsales

sundaydaySales <- filter(Spring_1617Sales, DAY == "Sunday")
sunsales <- sum (sundaydaySales$QUANTITY)
sunsales

weekendSales <- c(satsales*0.7,sunsales*0.7)
print (weekendSales)

#Halls Occupation
hallOccu <- read.csv("lecture_occupancy_new.csv",header=TRUE, sep=",")

#2015/16 Spring hall occupation
allOccu1617 <- filter (hallOccu, SEMESTER == "16_17_K")

saturdayOccu <- filter(allOccu1617, PAEV_ENG == "Saturday")
saturdayHallOccu <- distinct(saturdayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
satOccu <- sum (saturdayHallOccu$MAHUTAVUS)


sundayOccu <- filter(allOccu1617, PAEV_ENG == "Sunday")
sundayHallOccu <- distinct(sundayOccu,RUUM,TOIMUMINE,MAHUTAVUS)
sunOccu <- sum (sundayHallOccu$MAHUTAVUS)

weekendOccu <- c(satOccu*1.2,sunOccu*1.2)

counts <- table(weekendOccu, weekendSales)
barplot(counts, main="Density Co-orelation",xlab="Week Ends", col=c("darkblue","red"), beside=TRUE)

#plot(weekendSales,type = "o",col = "red", xlab = "Day", ylab = "Total",main = "Density Co-orelation" )+lines(weekendOccu, type = "o", col = "blue")


```


