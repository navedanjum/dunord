# DUNORD

## List of tasks:
1.	Understanding Business Goals   
2.	Understanding data   
3.	Data cleansing and transformation   
4.	Selection of predictive algorithms   
5.	Training various models on datasets   
6.	Testing and validation of models   
7.	Preparing Visualization   
8.	Prototype presentation    
9.	Posters and topic presentation to audience    

## Business Understanding 
1.	Our client Du Nord cafeteria seeks to increase the revenue from the products sold at the Liivi 2 cafeteria by predicting the demands so that they can order the enough food that can be sold. If they order the extra amount of product and it remain unsold then it causes waste of product as the items are food products and cannot be preserved. Currently, the cafeteria staff predicts the demand for products based on their experience and the schedule of lecture plans available to them.      
2.	The prediction based on the schedule plan for lecture can go wrong because there are number of factors that influence the students and the University staff to stop by at the cafeteria to buy the products. Therefore, Du Nord seeks an automatic method of predicting the demands taking into consideration the various factors that can help them lower the waste of food products and increase the revenue.     
3.	The success of this project is measured by the accurate demand prediction for the food products at the Du Nord cafeteria at J. Liivi 2 for the day. Good demand prediction will help the client to order precise amount of food for the day so that waste of food can be avoided.      



