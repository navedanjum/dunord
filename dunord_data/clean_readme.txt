Data cleaninsing steps:

1. Sale data not in tabular format and lot of discrepencies in the represented text
2. Cleaning out the unnecesssary header and transforming into an easy readable format such as csv
3. Removed all unnecessry entries 
4. Consistency in text representation
5. Changed the data on the basis of hours and the data (long format)
6. Removed the 0 and negative quantity entries
7. Some data are suspicious due to high sells on a single hour , require study of correlation or existece of a special event

Removed the following data with row counts
* 2015-2016 :  171 rows with 0 quantity is removed
* 2016-2017 :  154 rows with 0 quantity is removed & 35 rows containing missing values

Final cleaned version has a 3 files for the starting position:
* Lecture_occupancy csv file
* Sales data 2015-2016 csv file
* Sales data 2016-207 csv file

Summary of row counts in the cleaned datasets:
*****************************************************
> nrow(sales1516)
[1] 13780
> nrow(sales1617)
[1] 13941
> nrow(lecture_schedule)
[1] 21327


Good to go and test some prediction algorithms
* Represented sales data using gglot geompoints


